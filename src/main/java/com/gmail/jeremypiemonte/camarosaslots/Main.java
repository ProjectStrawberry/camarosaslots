package com.gmail.jeremypiemonte.camarosaslots;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.StringUtil;
import java.util.*;

public class Main extends JavaPlugin {
    Main plugin;
    FileConfiguration config;

    @Override
    public void onEnable() {
        plugin = this;

        plugin.getDataFolder().mkdirs();
        plugin.saveDefaultConfig();
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        this.config = plugin.getConfig();

        this.getCommand("slots").setExecutor(new CamarosaSlots(this));
        getLogger().info("Camarosa slots loaded.");
    }
}

class CamarosaSlots implements TabExecutor {
    Main plugin;
    public String[] roll100 = new String[100];
    public String[] roll500 = new String[100];
    public String[] roll1000 = new String[100];
    public String[] roll10000 = new String[100];
    public String[] roll50000 = new String[100];


    CamarosaSlots(Main plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        generateOdds();
    }
    FileConfiguration config = null;

    public void generateOdds() {
        String[] events = {"Loss", "Loss w/ token", "1x", "2x", "2.5x", "3x"};
        List<Integer> roll100Odds = config.getIntegerList("roll-100");
        List<Integer> roll500Odds = config.getIntegerList("roll-500");
        List<Integer> roll1000Odds = config.getIntegerList("roll-1000");
        List<Integer> roll10000Odds = config.getIntegerList("roll-10000");
        List<Integer> roll50000Odds = config.getIntegerList("roll-50000");

        int count = 0;

        for (int i = 0; i < events.length; i++) {
            for (int j = 0; j < roll100Odds.get(i); j++) {
                roll100[count] = events[i];
                count++;
            }
        }
        count = 0;
        for (int i = 0; i < events.length; i++) {
            for (int j = 0; j < roll500Odds.get(i); j++) {
                roll500[count] = events[i];
                count++;
            }
        }
        count = 0;
        for (int i = 0; i < events.length; i++) {
            for (int j = 0; j < roll1000Odds.get(i); j++) {
                roll1000[count] = events[i];
                count++;
            }
        }
        count = 0;
        for (int i = 0; i < events.length; i++) {
            for (int j = 0; j < roll10000Odds.get(i); j++) {
                roll10000[count] = events[i];
                count++;
            }
        }
        count = 0;
        for (int i = 0; i < events.length; i++) {
            for (int j = 0; j < roll50000Odds.get(i); j++) {
                roll50000[count] = events[i];
                count++;
            }
        }
    }

    public int[] gamble(int amount) { // money won, token, jackpot, number of icons to match
        int[] result = {0, 0, 0, 0};

        int randResult = (int) (Math.random() * 100);

        String[] odds = new String[100];
        if (amount == 100) odds = roll100;
        if (amount == 500) odds = roll500;
        if (amount == 1000) odds = roll1000;
        if (amount == 10000) odds = roll10000;
        if (amount == 50000) odds = roll50000;

        String outcome = odds[randResult];

        switch (outcome) {
            case "Loss":
                // wow so unlucky lol
                break;
            case "Loss w/ token":
                if (amount == 500) {
                    result[1] = (int) (Math.random() * 2);
                } else if (amount == 1000) {
                    result[1] = (int) (Math.random() * 3);
                } else if (amount == 10000) {
                    result[1] = (int) (Math.random() * 10);
                } else if (amount == 50000) {
                    result[1] = (int) (Math.random() * 20);
                } else {
                    result[1] = 1;
                }
                break;
            case "1x":
                result[0] = amount;
                result[3] = 2;
                break;
            case "2x":
                result[0] = amount * 2;
                result[3] = 3;
                break;
            case "2.5x":
                result[0] = (int) (amount * 2.5);
                result[3] = 4;
                break;
            case "3x":
                result[0] = amount * 3;
                result[2] = 1;
                result[3] = 5;
                break;
        }
        return result;
    }

    public String gambleSimulation(int amount, int balance, int timesGambled) {
        int totalGambles = 0;
        int tokens = 0;
        int jackpot = 0;
        int originalBal = balance;
        int losesWithNoTokens = 0;
        int tier1 = 0; // 2x
        int tier2 = 0; // 2.5x
        int brokeEven = 0;
        for (int i = timesGambled; i > 0 && balance > amount; i--) {
            balance = balance - amount;
            int[] result = gamble(amount);
            balance += result[0];
            tokens += result[1];
            if (result[0] == 0 && result[1] == 0) losesWithNoTokens++;
            if (result[0] == amount) brokeEven++;
            if (amount * 2 == result[0]) tier1++;
            if ((int) (amount * 2.5) == result[0]) tier2++;
            jackpot += result[2];
            totalGambles++;
        }
        int profit = (balance - originalBal);
        String profitText = (profit > 0 ? ChatColor.GREEN + "" + profit : ChatColor.RED + "" + profit);
        return "Stopped gambling with $" + ChatColor.YELLOW +  balance + ChatColor.WHITE + " left (" + totalGambles + " total gambles)\n" +
                "Profit: $" + profitText + ChatColor.WHITE + " | Tokens: " + tokens + " | Loses w/ no tokens: " + losesWithNoTokens + "\n" +
                "Broke even: " + brokeEven + " times | 2x: " + tier1 + " | 2.5x: " + tier2 + " | Jackpots: " + ChatColor.GOLD + jackpot;
    }
    public void sendCmd(String cmd) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
    }
    public String generateIcons(int[] result) {
        int matching = result[3];
        boolean token = result[1] >= 1;
        int randInt = (int) (Math.random() * 4);

        String tokenIcon = ChatColor.BOLD + "" + ChatColor.GOLD + "❀";
        String[] icons = {ChatColor.BOLD + "" + ChatColor.DARK_RED + "♡",
                ChatColor.BOLD + "" + ChatColor.DARK_GREEN + "♫",
                ChatColor.BOLD + "" + ChatColor.AQUA + "☂",
                ChatColor.BOLD + "" + ChatColor.YELLOW + "♤",
                ChatColor.BOLD + "" + ChatColor.LIGHT_PURPLE + "☆",
        };
        Collections.shuffle(Arrays.asList(icons));
        if (matching == 0 && !token) {
            // do nothing here
        } else if (matching == 0) {
            icons[randInt] = tokenIcon;
        } else if (matching == 2) {
            String matchingIcon = icons[randInt];
            for (int i = 0; i < icons.length - 3; i++) {
                icons[i] = matchingIcon;
            }
            if (randInt < 2) icons[randInt] = icons[(int) (Math.random() * 1)];
        } else if (matching == 3) {
            String matchingIcon = icons[randInt];
            for (int i = 0; i < icons.length - 2; i++) {
                icons[i] = matchingIcon;
            }
            if (randInt < 3) icons[randInt] = icons[(int) (Math.random() * 2)];
        } else if (matching == 4) {
            String matchingIcon = icons[randInt];
            for (int i = 0; i < icons.length - 1; i++) {
                icons[i] = matchingIcon;
            }
            if (randInt < 4) icons[randInt] = icons[(int) (Math.random() * 3)];
        } else if (matching == 5) {
            String matchingIcon = icons[randInt];
            Arrays.fill(icons, matchingIcon);
        }
        return Arrays.toString(icons).replace("[", "").replace("]", "").replace(",", "");
    }

    public void processGamble(Player player, int amount) {
        int[] result = gamble(amount);
        if (result[0] > 0 && result[2] != 1) {
            player.sendMessage(generateIcons(result) + " " + ChatColor.WHITE + " - " + ChatColor.GREEN + "You won $" + result[0] + "!");
            sendCmd("fe grant " + player.getName() + " " + result[0] + " silent");
        }
        if (result[1] >= 1) {
            int tokens = result[1];
            player.sendMessage(generateIcons(result) + " " + ChatColor.WHITE + " - " + ChatColor.GREEN + "You won " + tokens + "" + ChatColor.GOLD + " Casino token" + (tokens > 1 ? "s" : "") + "!");
            ItemStack item = new ItemStack(Material.SUNFLOWER);
            ItemMeta meta = item.getItemMeta();
            assert meta != null;
            meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Casino Token");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.YELLOW + "Redeem for special prizes at the");
            lore.add(ChatColor.YELLOW + "casino token room behind the bar!");
            meta.setLore(lore);
            meta.addEnchant(Enchantment.DURABILITY, 1, false);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(meta);
            for (int i = 0; i < tokens; i++) {
                player.getInventory().addItem(item);
            }
        }
        if (result[2] == 1) {
            sendCmd("fe grant " + player.getName() + " " + result[0] + " silent");
            player.sendMessage(generateIcons(result) + " " + ChatColor.WHITE + " - " + ChatColor.GREEN + "You hit a " + ChatColor.GOLD + "JACKPOT" + ChatColor.GREEN + " and won $" + result[0] + "!");
            if (!isVanished(player) && !player.hasPotionEffect(PotionEffectType.INVISIBILITY) && !player.getGameMode().toString().equals("SPECTATOR")) {
                spawnJackpotFireworks(player.getLocation(), 1);
            }
        }
        if (result[0] == 0 && result[1] == 0) {
            player.sendMessage(generateIcons(result) + " " + ChatColor.WHITE + " - " + ChatColor.RED + "You lost $" + amount);
        }
    }

    private boolean isVanished(Player player) {
        for (MetadataValue meta : player.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        return false;
    }

    public void spawnJackpotFireworks(Location location, int amount) {
        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.ORANGE, Color.YELLOW, Color.GREEN).flicker(true).build());

        fw.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
        fw.setFireworkMeta(fwm);
        fw.detonate();

        for (int i = 0; i < amount; i++) {
            Firework fw2 = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
            fw2.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
            fw2.setFireworkMeta(fwm);
        }
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You must be a player to use this command.");
            return true;
        }
        if (args.length > 0) {
            if ("reload".equals(args[0])) {
                if (!sender.hasPermission("camarosaslots.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                plugin.reloadConfig();
                plugin.config = plugin.getConfig();
                config = plugin.getConfig();
                generateOdds();
                sender.sendMessage(ChatColor.GOLD + "Slot odds have been reloaded.");
                return true;
            } else if ("gamble".equals(args[0])) {
                if (!sender.hasPermission("camarosaslots.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.DARK_RED + "ERROR: " + ChatColor.RED + "Not enough arguments were provided.");
                    return true;
                }
                Player player = (Player) sender;
                processGamble(player, Integer.parseInt(args[1]));
                return true;
            } else if ("sim".equals(args[0])) {
                if (!sender.hasPermission("camarosaslots.sim")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.DARK_RED + "ERROR: " + ChatColor.RED + "Not enough arguments were provided.");
                    return true;
                }
                int slotValue = Integer.parseInt(args[1]);
                if (slotValue != 100 && slotValue != 500 && slotValue != 1000 && slotValue != 10000 && slotValue != 50000) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + slotValue + " is not a valid slot value.");
                    return true;
                }

                String simOutcome = gambleSimulation(slotValue, Integer.parseInt(args[2]), Integer.parseInt(args[3]));
                sender.sendMessage(simOutcome);
                return true;
            }
        }
        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (sender.hasPermission("camarosaslots.admin")) {
            if (args.length == 1) {
                tabComplete.add("reload");
                tabComplete.add("sim");
            }
            if (args.length == 2 && args[0].equals("sim")) {
                tabComplete.add("[slot value]");
            }
            if (args.length == 3 && args[0].equals("sim")) {
                tabComplete.add("[starting balance]");
            }
            if (args.length == 4 && args[0].equals("sim")) {
                tabComplete.add("[amount of attempted rolls]");
            }
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
